import React from "react";
import ReactDOM from "react-dom";
import data from "./data";
import style from "./style.css";
function App() {
  return (
    <div>
      <h1>Курс валют</h1>
      <table border="1">
        <tbody>
          {data.map((element) => {
            return (
              <tr>
                <td>{element.txt}</td>
                <td>{element.rate}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
ReactDOM.render(<App></App>, document.getElementById("body"));
