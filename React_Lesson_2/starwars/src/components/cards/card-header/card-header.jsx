import React from "react";
import "./card-header.css"

const CardHeader = ({ Content }) => {
    let { name } = Content.Card;
  return <h2 className="card-header">{name}</h2>;
};

export default CardHeader;