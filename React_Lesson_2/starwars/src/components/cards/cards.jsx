import React from "react";
import data from "../data/data";

import CardBody from "./card-body/card-body";
import "./cards.css";

const Cards = () => {
  return (
    <div className="cards">
      {Array.isArray(data)
        ? data.map((element, index) => {
            return (
              <CardBody
                Card={element}
                key={Math.round(Math.random() * 1000 + index, 2) + element.name}
              ></CardBody>
            );
          })
        : false}
    </div>
  );
};
export default Cards;
