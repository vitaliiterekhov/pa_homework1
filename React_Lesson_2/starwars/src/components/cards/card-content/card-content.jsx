import React from "react";
import "./card-content.css";
import ImagePlaceholder from "./starwars.jpg";
const CardContent = ({ Content }) => {
  let {
    height,
    mass,
    skin_color,
    hair_color,
    eye_color,
    birth_year,
    gender,
    url,
  } = Content.Card;
  return (
    <>
      <img className="card-img" src={ImagePlaceholder} alt="{name}"></img>
      <ul className="card-content">
        <li>Height: {height}</li>
        <li>Mass: {mass}</li>
        <li>Skin color: {skin_color}</li>
        <li>Hair color: {hair_color}</li>
        <li>Eye color: {eye_color}</li>
        <li>Birth year: {birth_year}</li>
        <li>Gender: {gender}</li>
      </ul>
      <a href={url} className="link" target="_blank" rel="noreferrer">
        Learn More
      </a>
    </>
  );
};

export default CardContent;
