import React from "react";
import CardHeader from "../card-header/card-header";
import CardContent from "../card-content/card-content";
import "./card-body.css";

const CardBody = (Card) => {
  return (
    <div className="card-body">
      <CardHeader Content={Card}></CardHeader>
      <CardContent Content={Card}></CardContent>
    </div>
  );
};

export default CardBody;
