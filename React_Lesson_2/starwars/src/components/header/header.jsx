import React from "react";
import "./header.css";

const Header = () => {
    return (
        <h1 className="header">StarWars Card Game</h1>
    )
};
export default Header;