import React from "react";
import Header from "../header/header.jsx";
import Cards from "../cards/cards.jsx";
import "./app.css";

const App = () => {
  return (
    <>
          <Header></Header>
          <Cards></Cards>
    </>
  );
};

export default App;
